package models;

import static org.junit.Assert.*;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

public class UserClientTest {

	@Test
	public void purchaseTest() {
		UserClient owner = new UserClient("lgege", "luk_sw@hotmail.com",
				"password", "Lucas", "Test", "Como te llamas?",
				"Me llamo lucas test");
		UserClient client = new UserClient("gerger", "german_sw@hotmail.com",
				"password", "German", "Test", "Como te llamas vos?",
				"Me llamo german test");
		Set<Category> categories = new HashSet<Category>();
		categories.add(new Category("ropa"));
		categories.add(new Category("indumentaria"));
		Article article = new Article(20, 2320.2f, 
				"remeras", "remeras de test", new Date(), categories,
				new Status("new"), owner, true, null, null, null);
		owner.publish(article);
		client.purchase(article, 5, "donde nos encontramos?");
		assertEquals(15 , article.getStock());
	}

	@Test
	public void publishTest() {
		UserClient owner = new UserClient("lgege", "luk_sw@hotmail.com",
				"password", "Lucas", "Test", "Como te llamas?",
				"Me llamo lucas test");
		Set<Category> categories = new HashSet<Category>();
		categories.add(new Category("ropa"));
		categories.add(new Category("indumentaria"));
		assert (owner.getOwnedArticles().isEmpty());
		Article article = new Article(20, 2320.2f, 
				"remeras", "remeras de test", new Date(), categories,
				new Status("new"), owner, true, null, null, null);
		owner.publish(article);
		assertTrue(owner.getOwnedArticles().contains(article));
	}

	@Test
	public void askTest() {
		UserClient owner = new UserClient("lgege", "luk_sw@hotmail.com",
				"password", "Lucas", "Test", "Como te llamas?",
				"Me llamo lucas test");
		UserClient client = new UserClient("gerger", "german_sw@hotmail.com",
				"password", "German", "Test", "Como te llamas vos?",
				"Me llamo german test");
		Set<Category> categories = new HashSet<Category>();
		categories.add(new Category("ropa"));
		categories.add(new Category("indumentaria"));
		Article article = new Article(20, 2320.2f, 
				"remeras", "remeras de test", new Date(), categories,
				new Status("new"), owner, true, null, null, null);
		owner.publish(article);
		Query query1 = client.ask(article, "como funciona?");
		Query query2 = owner.ask(article, "me puedo mandar preguntas a mi mismo?");
		assertTrue(article.getQueries().contains(query1));
		assertNull(query2);
	}

	@Test
	public void answerTest() {
		UserClient owner = new UserClient("lgege", "luk_sw@hotmail.com",
				"password", "Lucas", "Test", "Como te llamas?",
				"Me llamo lucas test");
		UserClient client = new UserClient("gerger", "german_sw@hotmail.com",
				"password", "German", "Test", "Como te llamas vos?",
				"Me llamo german test");
		Set<Category> categories = new HashSet<Category>();
		categories.add(new Category("ropa"));
		categories.add(new Category("indumentaria"));
		Article article = new Article(20, 2320.2f, 
				"remeras", "remeras de test", new Date(), categories,
				new Status("new"), owner, true, null, null, null);
		owner.publish(article);
		Query query1 = client.ask(article, "como funciona?");
		assertNull (query1.getAnswer());
		owner.answer(query1, "ni idea,solo lo vendo");
		assertNotNull(query1.getAnswer());
	}

	@Test
	public void deleteQueryTest() {
		UserClient owner = new UserClient("lgege", "luk_sw@hotmail.com",
				"password", "Lucas", "Test", "Como te llamas?",
				"Me llamo lucas test");
		UserClient client1 = new UserClient("gerger", "german_sw@hotmail.com",
				"password", "German", "Test", "Como te llamas vos?",
				"Me llamo german test");
		UserClient client2 = new UserClient("mati", "mati@hotmail.com",
				"password", "Matias", "Test", "Como te llamas vos?",
				"Me llamo matias test");
		Set<Category> categories = new HashSet<Category>();
		categories.add(new Category("ropa"));
		categories.add(new Category("indumentaria"));
		Article article = new Article(20, 2320.2f, 
				"remeras", "remeras de test", new Date(), categories,
				new Status("new"), owner, true, null, null, null);
		owner.publish(article);
		Query query1 = client1.ask(article, "como funciona?");
		Query query2 = client2.ask(article, "es facil borrar comentarios?");
		client1.deleteQuery(query1);
		assertTrue(article.getQueries().contains(query1));
		owner.deleteQuery(query2);
		assertFalse(article.getQueries().contains(query2));
	}

	@Test
	public void reportUnacomplishedPurchaseTest() {
		UserClient owner = new UserClient("lgege", "luk_sw@hotmail.com",
				"password", "Lucas", "Test", "Como te llamas?",
				"Me llamo lucas test");
		UserClient client = new UserClient("gerger", "german_sw@hotmail.com",
				"password", "German", "Test", "Como te llamas vos?",
				"Me llamo german test");
		Set<Category> categories = new HashSet<Category>();
		categories.add(new Category("ropa"));
		categories.add(new Category("indumentaria"));
		Article article = new Article(20, 2320.2f, 
				"remeras", "remeras de test", new Date(), categories,
				new Status("new"), owner, true, null, null, null);
		owner.publish(article);
		Purchase purchase1 = client.purchase(article, 5,
				"donde nos encontramos?");
		Purchase purchase2 = client.purchase(article, 5,
				"donde nos encontramos?");
		client.reportUnacomplishedPurchase(purchase1);
		assertTrue(purchase1.getisReported());
		assertFalse(purchase1.getisConfirmed());
		client.confirmPurchase(purchase2);
		client.reportUnacomplishedPurchase(purchase2);
		assertFalse(purchase2.getisReported());
	}

	@Test
	public void confirmPurchaseTest() {
		UserClient owner = new UserClient("lgege", "luk_sw@hotmail.com",
				"password", "Lucas", "Test", "Como te llamas?",
				"Me llamo lucas test");
		UserClient client = new UserClient("gerger", "german_sw@hotmail.com",
				"password", "German", "Test", "Como te llamas vos?",
				"Me llamo german test");
		Set<Category> categories = new HashSet<Category>();
		categories.add(new Category("ropa"));
		categories.add(new Category("indumentaria"));
		Article article = new Article(20, 2320.2f, 
				"remeras", "remeras de test", new Date(), categories,
				new Status("new"), owner, true, null, null, null);
		owner.publish(article);
		Purchase purchase = client.purchase(article, 5,
				"donde nos encontramos?");
		assertFalse(purchase.getisConfirmed());
		client.confirmPurchase(purchase);
		assertTrue(purchase.getisConfirmed());
	}

}
