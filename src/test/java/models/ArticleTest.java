package models;

import static org.junit.Assert.*;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

public class ArticleTest {

	@Test
	public void getUnansweredQueriesTest() {
		UserClient owner = new UserClient("lgege","luk_sw@hotmail.com","password","Lucas","Test","Como te llamas?","Me llamo lucas test");
		Set<Category> categories = new HashSet<Category>();
		Set<Query> aux = null;
		categories.add(new Category("ropa"));
		categories.add(new Category("indumentaria"));
		Article article = new Article(20, 2320.2f, 
				"remeras", "remeras de test", new Date(), categories,
				new Status("new"), owner, true, null, null, null);
		owner.publish(article);
		UserClient client = new UserClient("gerger","german_sw@hotmail.com","password","German","Test","Como te llamas vos?","Me llamo german test");
		Query query1=client.ask(article,"es de buena calidad?");
		Query query2=client.ask(article,"alguna vez respondes alguna pregunta?");
		Query query3=client.ask(article,"hasta donde me tengo que ir para comprarla?");
		aux = article.getUnansweredQueries();
		assertTrue(aux.contains(query1));
		assertTrue(aux.contains(query2));
		assertTrue(aux.contains(query3));
		owner.answer(query2 , "cuando tenga ganas respondere");
		aux = article.getUnansweredQueries();
		assertTrue(aux.contains(query1));
		assertFalse(aux.contains(query2));
		assertTrue(aux.contains(query3));
	}

	@Test
	public void getSameOwnerTest() {
		UserClient owner = new UserClient("lgege","luk_sw@hotmail.com","password","Lucas","Test","Como te llamas?","Me llamo lucas test");
		Set<Category> categories = new HashSet<Category>();
		Set<Article> aux;
		categories.add(new Category("ropa"));
		categories.add(new Category("indumentaria"));
		Article article1 = new Article(20, 2320.2f, "remeras", "remeras de test", new Date(), categories, new Status("new"), owner, true, null, null, null);
		owner.publish(article1);
		Article article2 = new Article(42, 2130.2f, "cartas", "cartas de test", new Date(), categories, new Status("used"), owner, true, null, null, null);
		owner.publish(article2);
		Article article3 = new Article(1421, 2630.2f, "piedras", "piedras de test", new Date(), categories, new Status("used"), owner, true, null, null, null);
		owner.publish(article3);
		Article article4 = new Article(2230, 2306.2f, "piedras", "piedras de test", new Date(), categories, new Status("new"), owner, true, null, null, null);
		owner.publish(article4);
		aux = article1.getSameOwner(3);
		assertTrue(aux.contains(article2));
		assertTrue(aux.contains(article3));
		assertTrue(aux.contains(article4));
		assertFalse(aux.contains(article1));
		aux = article1.getSameOwner(6);
		assertTrue(aux.contains(article2));
		assertTrue(aux.contains(article3));
		assertTrue(aux.contains(article4));
		assertFalse(aux.contains(article1));
		aux = article2.getSameOwner(3);
		assertTrue(aux.contains(article3));
		assertTrue(aux.contains(article1));
		assertTrue(aux.contains(article4));
		assertFalse(aux.contains(article2));	
	}

	@Test
	public void sell() {
		UserClient owner = new UserClient("lgege","luk_sw@hotmail.com","password","Lucas","Test","Como te llamas?","Me llamo lucas test");
		Set<Category> categories = new HashSet<Category>();
		categories.add(new Category("ropa"));
		categories.add(new Category("indumentaria"));
		Article article = new Article(20, 2320.2f, "remeras", "remeras de test", new Date(), categories, new Status("new"), owner, true, null, null, null);
		owner.publish(article);
		assertEquals(20 ,article.getStock() );
		assertTrue(article.sell(5));
		assertEquals(15 , article.getStock());
		assertFalse(article.sell(30));
	}

	@Test
	public void cancelPurchase() {
		UserClient owner = new UserClient("lgege","luk_sw@hotmail.com","password","Lucas","Test","Como te llamas?","Me llamo lucas test");
		UserClient client = new UserClient("gerger","german_sw@hotmail.com","password","German","Test","Como te llamas vos?","Me llamo german test");
		Set<Category> categories = new HashSet<Category>();
		categories.add(new Category("ropa"));
		categories.add(new Category("indumentaria"));
		Article article = new Article(20, 2320.2f, "remeras", "remeras de test", new Date(), categories, new Status("new"), owner, true, null, null, null);
		owner.publish(article);
		Purchase purchase = client.purchase(article , 5, "donde nos encontramos?");
		article.cancelPurchase(purchase);
		assertEquals(20 ,article.getStock());
	}

}
