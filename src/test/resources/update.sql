ALTER TABLE userclient ADD admin boolean;
ALTER TABLE userclient ADD lastAccess timestamp without time zone;
ALTER TABLE userclient ADD banned boolean;
ALTER TABLE userclient ADD vip boolean;

UPDATE userclient SET admin=false, banned=false, vip=false, lastAccess=null;

ALTER TABLE article ADD suspended boolean;

UPDATE article SET suspended=false;