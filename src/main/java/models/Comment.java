package models;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Comment {

	@Column(nullable = false)
	private String comment;

	Comment() {
		// For Hibernate
	}

	public Comment(String comment) {
		this.comment = comment;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

}
