package models;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class HibernateStatusRepo extends AbstractHibernateRepo implements
		StatusRepo {

	@Autowired
	public HibernateStatusRepo(SessionFactory sessionFactory) {
		super(sessionFactory);
		// TODO Auto-generated constructor stub
	}

	public Set<Status> getAll() {
		List<Status> list = find("from Status");
		Set<Status> statuses = new HashSet<Status>();
		statuses.addAll(list);
		return statuses;
	}

	public Status getByName(String name) {
		List<Status> list = find("from Status where name=?", name);
		if (!list.isEmpty()) {
			return list.get(0);
		}
		return null;
	}

}
