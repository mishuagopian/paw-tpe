package models;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class HibernateArticleRepo extends AbstractHibernateRepo implements
		ArticleRepo {

	PurchaseRepo purchaseRepoInstance;
	UserRepo userRepoInstance;

	@Autowired
	public HibernateArticleRepo(SessionFactory sessionFactory,
			PurchaseRepo purchaseRepoInstance, UserRepo userRepoInstance) {
		super(sessionFactory);
		this.purchaseRepoInstance = purchaseRepoInstance;
		this.userRepoInstance = userRepoInstance;
	}

	public Article getById(int id) {
		return get(Article.class, id);
	}

	public List<Article> search(QueryObject queryObject) {
		String hql = "from Article where 1<2";
		List<Object> paramsList = new ArrayList<Object>();

		if (queryObject != null) {

			if (queryObject.getStatus() != null) {
				hql += " and status = ?";
				paramsList.add(queryObject.getStatus());
			}
			if (queryObject.getUser() != null) {
				hql += " and owner = ?";
				paramsList.add(queryObject.getUser());
			}
			if (queryObject.getName() != null) {
				hql += " and name like ?";
				paramsList.add("%" + queryObject.getName() + "%");
			}
			if (queryObject.getMinPrice() >= 0) {
				hql += " and price >= ?";
				paramsList.add(queryObject.getMinPrice());
			}
			if (queryObject.getMaxPrice() >= 0) {
				hql += " and price <= ?";
				paramsList.add(queryObject.getMaxPrice());
			}
			hql += "order by isprioritized desc";
			String order = (queryObject.isDescending() ? " desc" : " asc");
			if (queryObject.isOrderByVisits()) {
				hql += ", visits desc";
			}
			if (queryObject.isOrderByName()) {
				hql += ", name";
				hql += order;
			}
			if (queryObject.isOrderByPrice()) {
				hql += ", price";
				hql += order;
			}
			if (queryObject.isOrderByPublicationDate()) {
				hql += ", publicationdate";
				hql += order;
			}

		}
		Object[] params = paramsList.toArray();
		List<Article> articles = find(hql, params);
		if (queryObject != null && queryObject.getCategory() != null) {
			Set<Article> articlesOfCategory = queryObject.getCategory()
					.getArticles();
			articles.retainAll(articlesOfCategory);
		}
		return articles;
	}

	public List<Article> getMostVisited(int n) {
		List<Article> articles = find("from Article order by isprioritized desc, visits desc");
		int size = articles.size();
		int limit = n < size ? n : size;
		return articles.subList(0, limit);
	}

	public List<Article> getLastPublished(int n) {
		List<Article> articles = find("from Article order by isprioritized desc, publicationdate desc ");
		int size = articles.size();
		int limit = n < size ? n : size;

		return articles.subList(0, limit);
	}

	public Set<Article> getSuggestions(int n, Article article) {
		Set<UserClient> buyers = userRepoInstance.getBuyersOf(article);
		Map<Article, Integer> articles = new HashMap<Article, Integer>();
		for (UserClient buyer : buyers) {
			Set<Purchase> purchases = purchaseRepoInstance.getByBuyer(buyer);
			getAmountsOfPurchasesByArticle(articles, purchases);
		}
		return orderArticlesByPurchases(articles, n);
	}

	public Set<Article> getMostPurchased(int n) {
		Set<Purchase> purchases = purchaseRepoInstance.getAll();
		Map<Article, Integer> articles = new HashMap<Article, Integer>();
		getAmountsOfPurchasesByArticle(articles, purchases);
		return orderArticlesByPurchases(articles, n);
	}

	private void getAmountsOfPurchasesByArticle(Map<Article, Integer> articles,
			Set<Purchase> purchases) {
		for (Purchase purchase : purchases) {
			if (purchase.isConfirmed()) {
				if (articles.get(purchase.getArticle()) != null) {
					articles.put(
							purchase.getArticle(),
							articles.get(purchase.getArticle())
									+ purchase.getAmount());
				} else {
					articles.put(purchase.getArticle(), purchase.getAmount());
				}
			}
		}
	}

	private Set<Article> orderArticlesByPurchases(
			Map<Article, Integer> articles, int n) {
		Comparator<Article> comparator = new ValueComparator(articles);
		Map<Article, Integer> sortedArticles = new TreeMap<Article, Integer>(
				comparator);
		sortedArticles.putAll(articles);
		Set<Article> ans = new HashSet<Article>();
		int i = 0;
		for (Article article : sortedArticles.keySet()) {
			if (i < n) {
				ans.add(article);
				i++;
			} else {
				break;
			}
		}
		return ans;
	}

	private class ValueComparator implements Comparator<Article> {

		Map<Article, Integer> base;

		public ValueComparator(Map<Article, Integer> base) {
			this.base = base;
		}

		public int compare(Article a1, Article a2) {
			if (base.get(a1) >= base.get(a2)) {
				return -1;
			} else {
				return 1;
			}
		}

	}

	public void publish(Article article) {
		save(article);
	}
}
