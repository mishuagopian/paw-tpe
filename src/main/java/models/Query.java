package models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Query extends PersistentEntity {

	@Column(nullable = false)
	private String query;
	private String answer;

	@ManyToOne
	private Article article;

	@ManyToOne
	private UserClient creator;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable = false)
	private Date date;

	public final static int QUERY_MAX_SIZE = 255;
	public final static int ANSWER_MAX_SIZE = 255;
	
	
	Query() {
		// For Hibernate
	}

	public Query(String query, String answer, Article article,
			UserClient creator, Date date) {
		setProperties(query, answer, article, creator, date);
	}

	private void setProperties(String query, String answer, Article article,
			UserClient creator, Date date) {
		setQuery(query);
		answer(answer);
		setArticle(article);
		setCreator(creator);
		setDate(date);
	}

	public Date getDate() {
		return date;
	}

	private void setDate(Date date) {
		if (date == null) {
			throw new IllegalArgumentException("Date is null.");
		}
		this.date = date;
	}

	public String getQuery() {
		return query;
	}

	private void setQuery(String query) {
		if (query == null || query.length() > 256) {
			throw new IllegalArgumentException(
					"Queries must have no more than 256 characters");
		}
		this.query = query;
	}

	public String getAnswer() {
		return answer;
	}

	public void answer(String answer) {
		if (answer != null && answer.length() > 256) {
			throw new IllegalArgumentException(
					"Answers must have no more than 256 characters");
		}
		this.answer = answer;
	}

	public Article getArticle() {
		return article;
	}

	private void setArticle(Article article) {
		if (article == null) {
			throw new IllegalArgumentException("article is null");
		}
		this.article = article;
	}

	public UserClient getCreator() {
		return creator;
	}

	private void setCreator(UserClient creator) {
		if (creator == null) {
			throw new IllegalArgumentException("creator is null");
		}
		this.creator = creator;
	}

	@Override
	public int hashCode() {
		return 31 + query.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj.getClass() == this.getClass()) {
			if (((Query) obj).getQuery().equals(this.getQuery())
					&& ((Query) obj).getDate().equals(this.getDate())
					&& ((Query) obj).getCreator().equals(this.getCreator())
					&& ((Query) obj).getArticle().equals(this.getArticle())) {
				return true;
			}
			return false;
		}
		return false;
	}

}
