package models;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.persistence.Embeddable;
import javax.persistence.Lob;

import org.springframework.web.multipart.MultipartFile;

@Embeddable
public class ArticleImage implements MultipartFile {
	public static final String FORMAT_REGEX = "^(.png|.gif|.jpg|jpeg|.bmp)$";
	private String format;
	private String imgName;
	@Lob
	private byte[] img;

	ArticleImage() {

	}

	public ArticleImage(byte[] img, String format, String imgName) {
		setImg(img);
		setFormat(format);
		setImgName(imgName);
	}
	
	public String getImgName() {
		return imgName;
	}
	
	public byte[] getImg() {
		return img;
	}

	public String getFormat() {
		return format;
	}

	private void setImg(byte[] img) {
		if (img == null || img.length == 0) {
			throw new IllegalArgumentException();
		}
		this.img = img;
	}

	private void setImgName(String imgName) {
		if (imgName == null) {
			throw new IllegalArgumentException();
		}
		this.imgName = imgName;
	}

	private void setFormat(String format) {
		format=format.substring(format.length()-4, format.length());
		if (format == null || !format.matches(FORMAT_REGEX)) {
			throw new IllegalArgumentException();
		}
		this.format = format;
	}
	
	@SuppressWarnings("resource")
	public void transferTo(File dest) throws FileNotFoundException, IOException {
		new FileOutputStream(dest).write(img);
	}

	public String getName() {
		return imgName;
	}

	public String getOriginalFilename() {
		return getName();
	}

	public String getContentType() {
		return format;
	}

	public boolean isEmpty() {
		return img != null && img.length > 0;
	}

	public long getSize() {
		return img.length;
	}

	public byte[] getBytes() throws IOException {
		return img;
	}

	public InputStream getInputStream() throws IOException {
		return new ByteArrayInputStream(img);
	}
	

}
