package models;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class HibernatePurchaseRepo extends AbstractHibernateRepo implements
		PurchaseRepo {
	
	@Autowired
	public HibernatePurchaseRepo(SessionFactory sessionFactory) {
		super(sessionFactory);
	}

	public Set<Purchase> getAll() {
		Set<Purchase> purchases = new HashSet<Purchase>();
		List<Purchase> list = find("from Purchase");
		purchases.addAll(list);
		return purchases;
	}

	public Set<Purchase> getByBuyer(UserClient user) {
		Set<Purchase> purchases = new HashSet<Purchase>();
		List<Purchase> list = find("from Purchase where buyer=?", user);
		purchases.addAll(list);
		return purchases;
	}
	
	public Set<Purchase> getBySaler(UserClient user) {
		Set<Purchase> purchases = new HashSet<Purchase>();
		List<Purchase> list = find("from Purchase where article.owner=?", user);
		purchases.addAll(list);
		return purchases;
	}
	
	public Set<Purchase> getByArticle(Article article) {
		Set<Purchase> purchases = new HashSet<Purchase>();
		List<Purchase> list = find("from Purchase where article=?", article);
		purchases.addAll(list);
		return purchases;
	}
	
	public void purchase(Purchase purchase){
		save(purchase);
	}

	public Purchase getById(int id) {
		return get(Purchase.class, id);
	}
	
	@SuppressWarnings("deprecation")
	public List<Integer> getTotalAmount(Date from, Date to) {
		List<Integer> answer = new ArrayList<Integer>();
		Map<Date,Integer> purchasesPerDay = new HashMap<Date,Integer>();
		if(from.compareTo(to) > 0){
			return answer;
		}
		List<Date> dates = new ArrayList<Date>();
		Date aux1 = new Date(from.getTime());
		while(aux1.compareTo(to) <= 0){
			dates.add(aux1);
			aux1 = new Date(aux1.getTime()+TimeUnit.DAYS.toMillis(1));
		}
		for(Date d : dates){
			purchasesPerDay.put(d,0);
		}
		List<Purchase> purchases = find("from Purchase where purchaseDate >= ? and purchaseDate<= ? and isconfirmed is true",from , new Date(to.getTime()+TimeUnit.DAYS.toMillis(1)));
		for(Purchase p : purchases){
			Date aux2 = new Date(p.getPurchaseDate().getYear(), p.getPurchaseDate().getMonth(), p.getPurchaseDate().getDate());
			purchasesPerDay.put(aux2, purchasesPerDay.get(aux2)+p.getAmount());
		}
		for(Date d: dates){
			answer.add(purchasesPerDay.get(d));
		}
		return answer;
	}
	
}
