package models;

import java.util.Set;

public interface UserRepo {
	public UserClient getByUsername(String username);
	public UserClient getByEmail(String email);
	public UserClient signIn(String username, String password);
	public UserClient signUp(UserClient user);
	public Set<UserClient> getSellers();
	public Set<UserClient> getBuyersOf(Article article);
	public Set<UserClient> getAll();
	public void add(UserClient user);
	public void save(UserClient user);
	
}
