package models;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CollectionOfElements;

@Entity
public class UserClient extends PersistentEntity {

	public final static int EMAIL_MAX_SIZE = 255;
	public final static int COMMENT_MAX_SIZE = 255;
	public final static int FIRSTNAME_MAX_SIZE = 255;
	public final static int LASTNAME_MAX_SIZE = 255;
	public final static int PASSWORD_MAX_SIZE = 255;
	public final static int USERNAME_MAX_SIZE = 255;
	public final static int SECRET_QUESTION_MAX_SIZE = 255;
	public final static int SECRET_ANSWER_MAX_SIZE = 255;
	
	@Column(nullable = false, unique = true)
	private String username;

	@Column(nullable = false, unique = true)
	private String email;

	@Column(nullable = false)
	private String password;

	@Column(nullable = false)
	private String firstName;

	@Column(nullable = false)
	private String lastName;

	@Column(nullable = true)
	private String secretQuestion;

	@Column(nullable = true)
	private String secretQuestionAnswer;
	private int positiveVotes;
	private int neutralVotes;
	private int negativeVotes;
	private Boolean vip;
	private Boolean admin;
	private Boolean banned;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date lastAccess;

	@CollectionOfElements
	private List<Comment> commentsFromUsers = new ArrayList<Comment>();

	@OneToMany(mappedBy="owner", cascade=CascadeType.ALL)
	private Set<Article> ownedArticles = new HashSet<Article>();
	
	@OneToMany(mappedBy="buyer", cascade=CascadeType.ALL)
	private Set<Purchase> purchases = new HashSet<Purchase>();

	UserClient() {
		// For Hibernate
	}

	public UserClient(String username, String email, String password,
			String firstName, String lastName, String secretQuestion,
			String secretQuestionAnswer) {
		setId(-1);
		setProperties(username, email, password, firstName, lastName,
				secretQuestion, secretQuestionAnswer);
	}

	private void setProperties(String username, String email, String password,
			String firstName, String lastName, String secretQuestion,
			String secretQuestionAnswer) {
		setUsername(username);
		setEmail(email);
		setPassword(password);
		setFirstName(firstName);
		setLastName(lastName);
		setSecretQuestion(secretQuestion);
		setSecretQuestionAnswer(secretQuestionAnswer);
		neutralVotes = 0;
		positiveVotes = 0;
		negativeVotes = 0;
		vip = false;
		admin = false;
		banned = false;
		lastAccess = new Date();
	}

	public String getSecretQuestion() {
		return secretQuestion;
	}

	public void setSecretQuestion(String secretQuestion) {
		if (secretQuestion == null) {
			throw new IllegalArgumentException("Secret Question is null.");
		}
		if (secretQuestion.length() > 128) {
			throw new IllegalArgumentException(
					"Secret Question is too long, must have less than 128 characters.");
		}
		if (secretQuestion.length() == 0) {
			throw new IllegalArgumentException("Secret Question must be filled");
		}
		this.secretQuestion = secretQuestion;
	}
	
	public String getSecretQuestionAnswer() {
		return secretQuestionAnswer;
	}

	public void setSecretQuestionAnswer(String secretQuestionAnswer) {
		if (secretQuestionAnswer == null) {
			throw new IllegalArgumentException(
					"Secret Question Answer is null.");
		}
		if (secretQuestionAnswer.length() > 128) {
			throw new IllegalArgumentException(
					"Secret Question Answer is too long, must have less than 128 characters.");
		}
		if (secretQuestionAnswer.length() == 0) {
			throw new IllegalArgumentException(
					"Secret Question Answer must be filled");
		}
		this.secretQuestionAnswer = secretQuestionAnswer;
	}

	public int getPositiveVotes() {
		return positiveVotes;
	}

	public int getNeutralVotes() {
		return neutralVotes;
	}

	public int getNegativeVotes() {
		return negativeVotes;
	}

	public List<Comment> getCommentsFromUsers() {
		return commentsFromUsers;
	}

	public Set<Article> getOwnedArticles() {
		return ownedArticles;
	}

	public String getUsername() {
		return username;
	}
	
	public Boolean getAdmin() {
		return admin;
	}

	public Boolean getBanned() {
		return banned;
	}

	public Boolean getVip() {
		return vip;
	}
	
	public void changeVipAccess(UserClient admin, Boolean bool) {
		if (admin.getAdmin()) {
			vip = bool;
		}
	}

	public void changeBannedState(UserClient admin, Boolean bool) {
		if (admin.getAdmin()) {
			banned = bool;
		}
	}
	
	public void setUsername(String username) {
		if (username == null) {
			throw new IllegalArgumentException("Username is null.");
		}
		if (username.length() > 16) {
			throw new IllegalArgumentException(
					"Username is too long, must have less than 16 characters.");
		}
		if (username.length() == 0) {
			throw new IllegalArgumentException("Username must be filled");
		}
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		String emailreg = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$";
		if (email == null) {
			throw new IllegalArgumentException("Email is null.");
		}
		if (email.length() > 32) {
			throw new IllegalArgumentException(
					"Email is too long, must have less than 16 characters.");
		}
		if (username.length() == 0) {
			throw new IllegalArgumentException("Email must be filled");
		}
		if (!email.matches(emailreg)) {
			throw new IllegalArgumentException("Email is no valid.");
		}
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		if (password == null) {
			throw new IllegalArgumentException("Password is null.");
		}
		if (password.length() > 16) {
			throw new IllegalArgumentException(
					"Password is too long, must have less than 16 characters.");
		}
		if (password.length() == 0) {
			throw new IllegalArgumentException("Password must be filled");
		}
		this.password = password;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		if (firstName == null) {
			throw new IllegalArgumentException("Firstname is null.");
		}
		if (firstName.length() > 32) {
			throw new IllegalArgumentException(
					"Firstname is too long, must have less than 32 characters.");
		}
		if (firstName.length() == 0) {
			throw new IllegalArgumentException("Firstname must be filled");
		}
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		if (lastName == null) {
			throw new IllegalArgumentException("Lastname is null.");
		}
		if (lastName.length() > 32) {
			throw new IllegalArgumentException(
					"Lastname is too long, must have less than 32 characters.");
		}
		if (lastName.length() == 0) {
			throw new IllegalArgumentException("Lastname must be filled");
		}
		this.lastName = lastName;
	}

	@Override
	public int hashCode() {
		return 31 + username.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj.getClass() == this.getClass()) {
			if (((UserClient) obj).getUsername().equals(this.getUsername())) {
				return true;
			}
			return false;
		}
		return false;
	}
	
	public void votePositive(){
		positiveVotes++;
	}
	
	public void voteNegative(){
		negativeVotes++;
	}
	
	public void voteNeutral(){
		neutralVotes++;
	}
	
	public void addComment(Comment comment){
		commentsFromUsers.add(comment);
	}
	
	public void publish(Article article){
		ownedArticles.add(article);
	}
	
	public Purchase purchase(Article article, Integer amount, String comment) {
		if (!getBanned() && !article.getOwner().getBanned() && !article.getSuspended()) {
			article.sell(amount);
			Purchase purchase = new Purchase(amount, comment, new Date(), this, article);
			purchases.add(purchase);
			return purchase;			
		}
		return null;
	}
	
	public void confirmPurchase(Purchase purchase){
		if(!purchase.isConfirmed() && !purchase.getisReported() && purchase.getBuyer().equals(this)){
			purchase.confirm();
		}
	}

	public void reportUnacomplishedPurchase(Purchase purchase) {
		if(!purchase.isConfirmed() && !purchase.getisReported() && purchase.getBuyer().equals(this)){
			purchase.report();
		}
	}

	public Query ask(Article article, String question) {
		if (!article.getOwner().equals(this)) {
			Query query = new Query(question, null, article, this, new Date());
			article.ask(query);
			return query;
		}
		return null;
	}

	public void answer(Query query, String answer) {
		if (query.getArticle().getOwner().equals(this)) {
			query.answer(answer);
		}
	}

	public void deleteQuery(Query query) {
		if (query.getArticle().getOwner().equals(this)) {
			query.getArticle().deleteQuery(query);
		}		
	}
	
	public void grantVipAccess(UserClient user) {
		if (this.getAdmin()) {
			user.changeVipAccess(this, true);
		}		
	}

	public void revokeVipAccess(UserClient user) {
		if (this.getAdmin()) {
			user.changeVipAccess(this, false);
		}		
	}

	public void ban(UserClient user) {
		if (this.getAdmin()) {
			user.changeBannedState(this, true);
		}		
	}

	public void unban(UserClient user) {
		if (this.getAdmin()) {
			user.changeBannedState(this, false);
		}		
	}
	
	public void suspend(Article article) {
		if (article.getOwner().equals(this)) {
			article.suspend();
		}
	}

	public boolean checkPassword(String password) {
		return this.password.equals(password);
	}

	public void access() {
		this.lastAccess = new Date();
	}
	
	public Date getLastAccess() {
		return lastAccess;
	}

	public void unsuspend(Article article) {
		if (article.getOwner().equals(this)) {
			article.unsuspend();
		}		
	}
	
}
