package models;

import java.util.Set;

public interface CategoryRepo {
	
	public Set<Category> getAll();
	
	public Category getByName(String name);
	
}
