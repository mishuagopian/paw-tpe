package models;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

@Entity
public class Status extends PersistentEntity {

	@Column(nullable = false)
	private String name;

	@OneToMany(mappedBy = "status", cascade=CascadeType.ALL)
	private Set<Article> articles = new HashSet<Article>();

	Status() {
		// For Hibernate
	}

	public Status(String name) {
		setId(-1);
		setProperties(name);
	}

	private void setProperties(String name) {
		setName(name);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		if (name == null) {
			throw new IllegalArgumentException("Status name is null.");
		}
		if (name.length() > 32) {
			throw new IllegalArgumentException(
					"Status name is too long, must have less then 32 characters.");
		}
		if (name.length() == 0) {
			throw new IllegalArgumentException("Status name must be filled");
		}
		this.name = name;
	}

	@Override
	public int hashCode() {
		return 31 + name.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj.getClass() == this.getClass()) {
			if (((Status) obj).getName().equals(this.getName())) {
				return true;
			}
			return false;
		}
		return false;
	}
}
