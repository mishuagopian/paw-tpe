package web.pages.userPublicProfile;

import models.UserClient;

import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;

@SuppressWarnings("serial")
public class VipPanel extends Panel {

	public VipPanel(String id, IModel<UserClient> model) {
		super(id, model);
	}
	
	@Override
	protected void onBeforeRender() {
		setVisible(((UserClient)getDefaultModelObject()).getVip());
		super.onBeforeRender();
	}
}
