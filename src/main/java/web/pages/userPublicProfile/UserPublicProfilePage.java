package web.pages.userPublicProfile;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import models.Article;
import models.Comment;
import models.EntityModel;
import models.EntityResolver;
import models.UserClient;
import models.UserRepo;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.markup.repeater.RefreshingView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.model.StringResourceModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.spring.injection.annot.SpringBean;

import web.pages.base.ArticleLinkPanel;
import web.pages.base.BasePage;
import web.pages.home.HomePage;

@SuppressWarnings("serial")
public class UserPublicProfilePage extends BasePage {
	
	@SpringBean
	private EntityResolver entityResolver;
	
	@SpringBean
	private UserRepo userRepoInstance;
	
	public UserPublicProfilePage(PageParameters params) {
		UserClient user = userRepoInstance.getByUsername(params.get("username").toString());
		if (user == null) {
			setResponsePage(HomePage.class);
		}
		add(new VipPanel("vipUser", new EntityModel<UserClient>(UserClient.class, user)));
		add(new Label("username", new PropertyModel<String>(new EntityModel<UserClient>(UserClient.class, user), "username")));
		add(new Label("firstname", new PropertyModel<String>(new EntityModel<UserClient>(UserClient.class, user), "firstName")));
		add(new Label("lastname", new PropertyModel<String>(new EntityModel<UserClient>(UserClient.class, user), "lastName")));
		add(new Label("positiveVotes", new PropertyModel<Integer>(new EntityModel<UserClient>(UserClient.class, user), "positiveVotes")));
		add(new Label("negativeVotes", new PropertyModel<Integer>(new EntityModel<UserClient>(UserClient.class, user), "negativeVotes")));
		add(new Label("neutralVotes", new PropertyModel<Integer>(new EntityModel<UserClient>(UserClient.class, user), "neutralVotes")));
		
		add(new Label("usernameArticles", new StringResourceModel("usernameArticles", this, new EntityModel<UserClient>(UserClient.class, user))));
		add(new RefreshingView<Article>("article", new EntityModel<UserClient>(UserClient.class, user)) {
			private static final long serialVersionUID = 1L;

			@Override
			protected Iterator<IModel<Article>> getItemModels() {
				List<IModel<Article>> result = new ArrayList<IModel<Article>>();
				for (Article a :((UserClient)getDefaultModelObject()).getOwnedArticles()) {
					final int id = a.getId(); 
					result.add(new LoadableDetachableModel<Article>() {
						private static final long serialVersionUID = 1L;

						@Override
						protected Article load() {
							return entityResolver.fetch(Article.class, id);
						}
					});
				}
				return result.iterator();
			}

			@Override
			protected void populateItem(Item<Article> item) {
				item.add(new ArticleLinkPanel("articleLink", item.getModel()));
			}
		});
		
		add(new Label("usernameReceivedComments", new StringResourceModel("usernameReceivedComments", this, new EntityModel<UserClient>(UserClient.class, user))));
		PropertyModel<List<Comment>> comments = new PropertyModel<List<Comment>>(new EntityModel<UserClient>(UserClient.class, user), "commentsFromUsers");
		add(new ListView<Comment> ("comment", comments) {
			@Override
			protected void populateItem(ListItem<Comment> item) {
				item.add(new Label("commentDescription", new PropertyModel<String>(item.getModel(), "comment")));
			}
		});
	}

}
