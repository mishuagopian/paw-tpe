package web.pages.admin;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import models.Category;
import models.CategoryRepo;
import models.EntityModel;
import models.EntityResolver;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.markup.repeater.RefreshingView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import web.pages.secured.AdminPage;

@SuppressWarnings("serial")
public class PurchasesByCategoryPage extends AdminPage {
	
	@SpringBean
	private CategoryRepo categoryRepoInstance;
	
	@SpringBean
	private EntityResolver entityResolver;
	
	public PurchasesByCategoryPage() {
		add(new RefreshingView<Category>("category", new EntityModel<Category>(Category.class)) {
			private static final long serialVersionUID = 1L;
			
			@Override
			protected Iterator<IModel<Category>> getItemModels() {
				List<IModel<Category>> result = new ArrayList<IModel<Category>>();
				for (Category c :categoryRepoInstance.getAll()) {
					final int id = c.getId(); 
					result.add(new LoadableDetachableModel<Category>() {
						private static final long serialVersionUID = 1L;
						
						@Override
						protected Category load() {
							return entityResolver.fetch(Category.class, id);
						}	
					});
				}
				return result.iterator();
			}

			@Override
			protected void populateItem(Item<Category> item) {
				item.add(new Label("name", item.getModelObject().getName()));
				item.add(new Label("purchases", item.getModelObject().getPurchases().toString()));
			}
		});
	}
}

