package web.pages.admin;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import models.EntityModel;
import models.EntityResolver;
import models.UserClient;
import models.UserRepo;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.markup.repeater.RefreshingView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.model.StringResourceModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import web.pages.base.DatePanel;
import web.pages.secured.AdminPage;

@SuppressWarnings("serial")
public class UsersLastAccessPage extends AdminPage {
	
	@SpringBean
	private UserRepo userRepoInstance;
	
	@SpringBean
	private EntityResolver entityResolver;
	
	public UsersLastAccessPage(){
		add(new RefreshingView<UserClient>("user", new EntityModel<UserClient>(UserClient.class)) {
			private static final long serialVersionUID = 1L;

			@Override
			protected Iterator<IModel<UserClient>> getItemModels() {
				List<IModel<UserClient>> result = new ArrayList<IModel<UserClient>>();
				for (UserClient u :userRepoInstance.getAll()) {
					final int id = u.getId(); 
					result.add(new LoadableDetachableModel<UserClient>() {
						private static final long serialVersionUID = 1L;

						@Override
						protected UserClient load() {
							return entityResolver.fetch(UserClient.class, id);
						}
					});
				}
				return result.iterator();
			}

			@Override
			protected void populateItem(Item<UserClient> item) {
				item.add(new Label("username", item.getModel().getObject().getUsername()));
				StringResourceModel srm;
				if (item.getModelObject().getAdmin()){
					srm = new StringResourceModel("yes", this, null);
				} else {
					srm = new StringResourceModel("no", this, null);
				}
				item.add(new Label("admin", srm));
				if (item.getModelObject().getLastAccess() != null) {
					item.add(new DatePanel("accessDate", new PropertyModel<Date>(item.getModel(),"lastAccess")));	
				} else {
					item.add(new Label("accessDate", new StringResourceModel("noLastAccessData", this, null)));
				}
			}
		});
	}
}
