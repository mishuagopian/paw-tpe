package web.pages.admin;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import models.EntityModel;
import models.EntityResolver;
import models.UserClient;
import models.UserRepo;

import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.markup.repeater.RefreshingView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import web.pages.secured.AdminPage;

@SuppressWarnings("serial")
public class GrantVipAccessPage extends AdminPage {
	
	@SpringBean
	private UserRepo userRepoInstance;
	
	@SpringBean
	private EntityResolver entityResolver;
	
	public GrantVipAccessPage(){
		add(new RefreshingView<UserClient>("user", new EntityModel<UserClient>(UserClient.class)) {
			private static final long serialVersionUID = 1L;

			@Override
			protected Iterator<IModel<UserClient>> getItemModels() {
				List<IModel<UserClient>> result = new ArrayList<IModel<UserClient>>();
				for (UserClient u :userRepoInstance.getAll()) {
					final int id = u.getId(); 
					result.add(new LoadableDetachableModel<UserClient>() {
						private static final long serialVersionUID = 1L;

						@Override
						protected UserClient load() {
							return entityResolver.fetch(UserClient.class, id);
						}
					});
				}
				return result.iterator();
			}

			@Override
			protected void populateItem(Item<UserClient> item) {
				item.add(new UserGrantVipAccessPanel("userGrantAccess",item.getModel()));
			}
		});
	}
}
