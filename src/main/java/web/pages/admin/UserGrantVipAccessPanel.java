package web.pages.admin;

import models.UserClient;
import models.UserRepo;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import web.PawShopSession;

@SuppressWarnings("serial")
public class UserGrantVipAccessPanel extends Panel {
	
	@SpringBean
	private UserRepo userRepoInstance;

	public UserGrantVipAccessPanel(String id, IModel<UserClient> model) {
		super(id, model);
		add(new Label("username", model.getObject().getUsername()));
		Link<UserClient> grantLink = new Link<UserClient>("grant", model){
			@Override
			public void onClick() {
				PawShopSession s = PawShopSession.get();
				UserClient user = userRepoInstance.getByUsername(s.getUsername());
				user.grantVipAccess(getModelObject());
			}
			
			@Override
			protected void onBeforeRender() {
				setVisible(!getModelObject().getVip());
				super.onBeforeRender();
			}
		};
		add(grantLink);
		Link<UserClient> revokeLink = new Link<UserClient>("revoke", model){
			@Override
			public void onClick() {
				PawShopSession s = PawShopSession.get();
				UserClient user = userRepoInstance.getByUsername(s.getUsername());
				user.revokeVipAccess(getModelObject());
			}
			
			@Override
			protected void onBeforeRender() {
				setVisible(getModelObject().getVip());
				super.onBeforeRender();
			}
		};
		add(revokeLink);
	}

}
