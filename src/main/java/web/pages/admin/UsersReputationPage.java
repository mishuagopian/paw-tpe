package web.pages.admin;

import java.util.Comparator;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

import models.EntityModel;
import models.EntityResolver;
import models.UserClient;
import models.UserRepo;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.markup.repeater.RefreshingView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import web.pages.secured.AdminPage;

@SuppressWarnings("serial")
public class UsersReputationPage extends AdminPage {
	@SpringBean
	private UserRepo userRepoInstance;

	@SpringBean
	private EntityResolver entityResolver;
	private int order;

	public UsersReputationPage(int order) {
		this.order = order;
		
		add(new Link<Void>("positiveLink") {
			@Override
			public void onClick() {
				setResponsePage(new UsersReputationPage(0));
			}
		});
		add(new Link<Void>("negativeLink") {
			@Override
			public void onClick() {
				setResponsePage(new UsersReputationPage(1));
			}
		});
		add(new Link<Void>("differenceLink") {
			@Override
			public void onClick() {
				setResponsePage(new UsersReputationPage(2));
			}
		});
		
		add(new RefreshingView<UserClient>("user", new EntityModel<UserClient>(
				UserClient.class)) {
			private static final long serialVersionUID = 1L;

			@Override
			protected Iterator<IModel<UserClient>> getItemModels() {
				Set<IModel<UserClient>> result = new TreeSet<IModel<UserClient>>(
						new Comparator<IModel<UserClient>>() {

							public int compare(IModel<UserClient> o1,
									IModel<UserClient> o2) {
								int ret = 0;
								if (UsersReputationPage.this.order == 0) {
									ret = (o2.getObject().getPositiveVotes() - o1
											.getObject().getPositiveVotes());
									return ret == 0 ? 1 : ret;
								} else if (UsersReputationPage.this.order == 1) {
									ret = o2.getObject().getNegativeVotes()
											- o1.getObject().getNegativeVotes();
									return ret == 0 ? 1 : ret;
								} else {
									ret =(o2.getObject().getPositiveVotes() - o2
											.getObject().getNegativeVotes())
											- (o1.getObject()
													.getPositiveVotes() - o1
													.getObject()
													.getNegativeVotes());
									return ret == 0 ? 1 : ret;
								}
							}
						});
				for (UserClient u : userRepoInstance.getAll()) {
					final int id = u.getId();
					result.add(new LoadableDetachableModel<UserClient>() {
						private static final long serialVersionUID = 1L;

						@Override
						protected UserClient load() {
							return entityResolver.fetch(UserClient.class, id);
						}
					});
				}
				return result.iterator();
			}

			@Override
			protected void populateItem(Item<UserClient> item) {
				item.add(new Label("username", item.getModel().getObject()
						.getUsername()));
				PropertyModel<Integer> pos = new PropertyModel<Integer>(
						item.getModel(), "positiveVotes");
				PropertyModel<Integer> neg = new PropertyModel<Integer>(
						item.getModel(), "negativeVotes");
				item.add(new Label("positiveVotes", pos));
				item.add(new Label("negativeVotes", neg));
				item.add(new Label("difference", String.valueOf(pos.getObject()
						- neg.getObject())));
			}
		});
	}
}
