package web.pages.signUp;

import models.EntityResolver;
import models.UserClient;
import models.UserRepo;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.wicket.extensions.markup.html.captcha.CaptchaImageResource;
import org.apache.wicket.markup.ComponentTag;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.PasswordTextField;
import org.apache.wicket.markup.html.form.RequiredTextField;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.image.NonCachingImage;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.model.ResourceModel;
import org.apache.wicket.model.StringResourceModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.apache.wicket.util.value.ValueMap;
import org.apache.wicket.validation.IValidatable;
import org.apache.wicket.validation.IValidator;
import org.apache.wicket.validation.ValidationError;
import org.apache.wicket.validation.validator.EmailAddressValidator;
import org.apache.wicket.validation.validator.StringValidator.MaximumLengthValidator;

import web.pages.base.BasePage;
import web.pages.signIn.SignInPage;

@SuppressWarnings("serial")
public class SignUpPage extends BasePage  {

	@SpringBean
	private UserRepo userRepoInstance;
	
	@SpringBean
	private EntityResolver entityResolver;
	
	private transient String username;
	private transient String password;
	private transient String firstname;
	private transient String lastname;
	private transient String email;
	private transient String secretQuestion;
	private transient String secretAnswer;
	
	public SignUpPage() {
		add(new FeedbackPanel("feedback"));
		Form<SignUpPage> form = new Form<SignUpPage>("signUpForm", new CompoundPropertyModel<SignUpPage>(this)) {
			@Override
			protected void onSubmit() {
				UserClient user = new UserClient(username, email, password, firstname, lastname, secretQuestion, secretAnswer);
				user = userRepoInstance.signUp(user);
				if (user != null) {
					setResponsePage(SignInPage.class);	
				} else {
					error(new StringResourceModel("credentialsInUse", this, null));
				}
			}
		};
		form.add(new TextField<String>("secretAnswer").add(new MaximumLengthValidator(UserClient.SECRET_ANSWER_MAX_SIZE)).setRequired(true));
		form.add(new TextField<String>("secretQuestion").add(new MaximumLengthValidator(UserClient.SECRET_ANSWER_MAX_SIZE)).setRequired(true));
		form.add(new TextField<String>("email").add(new MaximumLengthValidator(UserClient.EMAIL_MAX_SIZE)).setRequired(true).add(EmailAddressValidator.getInstance()));
		form.add(new TextField<String>("lastname").add(new MaximumLengthValidator(UserClient.LASTNAME_MAX_SIZE)).setRequired(true));
		form.add(new TextField<String>("firstname").add(new MaximumLengthValidator(UserClient.FIRSTNAME_MAX_SIZE)).setRequired(true));
		form.add(new TextField<String>("username").add(new MaximumLengthValidator(UserClient.USERNAME_MAX_SIZE)).setRequired(true));
		form.add(new PasswordTextField("password").add(new MaximumLengthValidator(UserClient.PASSWORD_MAX_SIZE)).setRequired(true));
		form.add(new Button("submit", new ResourceModel("submit")));
		addCaptchaToForm(form);
		add(form);
	}
	
	public void addCaptchaToForm(Form<SignUpPage> form){
		final String imagePass = RandomStringUtils.randomNumeric(7);
		final ValueMap properties = new ValueMap();
		final CaptchaImageResource captchaImageResource;
		captchaImageResource = new CaptchaImageResource(imagePass);
		form.add(new NonCachingImage("captchaImage", captchaImageResource));
		RequiredTextField<String> captcha = new RequiredTextField<String>("captcha", new PropertyModel<String>(properties,
				"captcha"))
				{
			@Override
			protected final void onComponentTag(final ComponentTag tag)
			{
				super.onComponentTag(tag);
				tag.put("value", "");
			}
				};
		form.add(captcha);
		IValidator<String> captchaValidator = new IValidator<String>(){
			public void validate(IValidatable<String> validatable) {
				final String field = validatable.getValue();
				if(!field.equals(imagePass)){
					validatable.error(new ValidationError().addMessageKey("invalidCaptcha"));
				}
			}
		};
		captcha.add(captchaValidator);
	}
	
}