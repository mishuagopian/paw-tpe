package web.pages.secured;

import models.UserRepo;

import org.apache.wicket.spring.injection.annot.SpringBean;

import web.PawShopSession;
import web.pages.signIn.SignInPage;

@SuppressWarnings("serial")
public class AdminPage extends SecuredPage {
	
	@SpringBean
	private UserRepo userRepoInstance;
	
	public AdminPage(){
		super();
		PawShopSession session = PawShopSession.get();
		if (!session.isAdmin(userRepoInstance)) {
			redirectToInterceptPage(new SignInPage());	
		}
	}
}
