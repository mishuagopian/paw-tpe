package web.pages.signIn;

import models.EntityResolver;
import models.UserClient;
import models.UserRepo;

import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.PasswordTextField;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.ResourceModel;
import org.apache.wicket.model.StringResourceModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.apache.wicket.validation.validator.StringValidator.MaximumLengthValidator;

import web.PawShopSession;
import web.pages.base.BasePage;
import web.pages.home.HomePage;

@SuppressWarnings("serial")
public class SignInPage extends BasePage  {

	@SpringBean
	private UserRepo userRepoInstance;
	
	@SpringBean
	private EntityResolver entityResolver;
	
	private transient String username;
	private transient String password;
	
	public SignInPage() {
		add(new FeedbackPanel("feedback"));
		Form<SignInPage> form = new Form<SignInPage>("signInForm", new CompoundPropertyModel<SignInPage>(this)) {
			@Override
			protected void onSubmit() {
				PawShopSession s = PawShopSession.get();
				if (s.signIn(username, password, userRepoInstance)) {
					if (!continueToOriginalDestination()) {
						setResponsePage(HomePage.class);
					}
				} else {
					error(new StringResourceModel("invalidCredentials", this, null).getString());
				}
			}
		};
		form.add(new TextField<String>("username").add(new MaximumLengthValidator(UserClient.USERNAME_MAX_SIZE)).setRequired(true));
		form.add(new PasswordTextField("password").add(new MaximumLengthValidator(UserClient.PASSWORD_MAX_SIZE)).setRequired(true));
		form.add(new Button("submit", new ResourceModel("submit")));
		add(form);
	}
	
}
