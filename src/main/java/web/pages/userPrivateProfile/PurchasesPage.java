package web.pages.userPrivateProfile;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import models.EntityModel;
import models.EntityResolver;
import models.Purchase;
import models.PurchaseRepo;
import models.UserClient;
import models.UserRepo;

import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.markup.repeater.RefreshingView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import web.PawShopSession;
import web.pages.secured.SecuredPage;

@SuppressWarnings("serial")
public class PurchasesPage extends SecuredPage {
	@SpringBean
	private EntityResolver entityResolver;

	@SpringBean
	private UserRepo userRepoInstance;
	
	@SpringBean
	private PurchaseRepo purchaseRepoInstance;

	public PurchasesPage() {
		PawShopSession session = PawShopSession.get();
		String username = session.getUsername();
		UserClient user = userRepoInstance.getByUsername(username);

		add(new RefreshingView<Purchase>("purchases", new EntityModel<UserClient>(
				UserClient.class, user)) {

			private static final long serialVersionUID = 1L;

			@Override
			protected Iterator<IModel<Purchase>> getItemModels() {
				List<IModel<Purchase>> result = new ArrayList<IModel<Purchase>>();
				for (Purchase p : purchaseRepoInstance.getByBuyer((UserClient)getDefaultModel().getObject())) {
					final int id = p.getId();
					result.add(new LoadableDetachableModel<Purchase>() {
						private static final long serialVersionUID = 1L;

						@Override
						protected Purchase load() {
							return entityResolver.fetch(Purchase.class, id);
						}
					});
				}
				return result.iterator();
			}

			@Override
			protected void populateItem(Item<Purchase> item) {
				item.add(new PurchasePanel("purchase", item.getModel()));
			}
		});
	}
}
