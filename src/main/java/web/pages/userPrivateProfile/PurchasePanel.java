package web.pages.userPrivateProfile;

import java.util.Date;

import models.Article;
import models.Purchase;
import models.UserClient;
import models.UserRepo;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.model.StringResourceModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.spring.injection.annot.SpringBean;

import web.PawShopSession;
import web.pages.article.ArticlePage;
import web.pages.base.DatePanel;
import web.pages.userPublicProfile.UserPublicProfilePage;

@SuppressWarnings("serial")
public class PurchasePanel extends Panel {
	
	@SpringBean
	private UserRepo userRepoInstance;

	public PurchasePanel(String id, IModel<Purchase> model) {
		super(id);
		initialize(model);
	}

	private void initialize(IModel<Purchase> model) {
		add(new DatePanel("purchaseDate", new PropertyModel<Date>(model,
				"purchaseDate")));
		
		PropertyModel<Article> articleModel = new PropertyModel<Article>(model,"article"); 
		Link<ArticlePage> articleLink = new BookmarkablePageLink<ArticlePage>("articleLink", ArticlePage.class, 
				new PageParameters().add("articleId", model.getObject().getArticle().getId()));
		articleLink.add(new Label("articleName",  new PropertyModel<String>(articleModel, "name")));
		add(articleLink);

		add(new Label("amount", new PropertyModel<Integer>(model, "amount")));

		PropertyModel<UserClient> buyerModel = new PropertyModel<UserClient>(model,"buyer");
		add(new Label("buyerUsername", new PropertyModel<String>(buyerModel,"username")));
		
		final PropertyModel<UserClient> sellerModel = new PropertyModel<UserClient>(articleModel,"owner"); 
		Link<UserPublicProfilePage> sellerLink = new BookmarkablePageLink<UserPublicProfilePage>("ownerLink", UserPublicProfilePage.class, 
				new PageParameters().add("username", model.getObject().getArticle().getOwner().getUsername()));
		sellerLink.add(new Label("ownerUsername",  new PropertyModel<String>(sellerModel, "username")));
		add(sellerLink);

		String srm;
		if (model.getObject().isConfirmed()) {
			srm = "yes";
		} else {
			srm = "no";
		}
		add(new Label("confirmed", new StringResourceModel(srm, this, null)));
		
		if (model.getObject().getisReported()) {
			srm = "yes";
		} else {
			srm = "no";
		}
		add(new Label("reported", new StringResourceModel(srm, this, null)));
		
		Link<Purchase> reportLink = new Link<Purchase>("report", model){
			@Override
			public void onClick() {
				PawShopSession s = PawShopSession.get();
				UserClient user = userRepoInstance.getByUsername(s.getUsername());
				user.reportUnacomplishedPurchase(getModelObject());
			}
			
			@Override
			protected void onBeforeRender() {
				PawShopSession s = PawShopSession.get();
				setVisible(!getModelObject().getisConfirmed() && !getModelObject().getisReported() && s.isSignedIn() && getModelObject().getBuyer().getUsername().equals(s.getUsername()));
				super.onBeforeRender();
			}
		};
		add(reportLink);
		
		Link<Purchase> confirmLink = new Link<Purchase>("confirm", model){
			@Override
			public void onClick() {
				PawShopSession s = PawShopSession.get();
				UserClient user = userRepoInstance.getByUsername(s.getUsername());
				user.confirmPurchase(getModelObject());
				setResponsePage(new ConfirmPurchasePage(getModel()));
			}
			
			@Override
			protected void onBeforeRender() {
				PawShopSession s = PawShopSession.get();
				setVisible(!getModelObject().getisConfirmed() && !getModelObject().getisReported() && s.isSignedIn() && getModelObject().getBuyer().getUsername().equals(s.getUsername()));
				super.onBeforeRender();
			}
		};
		add(confirmLink);
		

	}
}
