package web.pages.userPrivateProfile;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import models.Article;
import models.ArticleRepo;
import models.EntityModel;
import models.EntityResolver;
import models.UserClient;
import models.UserRepo;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.markup.repeater.RefreshingView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.StringResourceModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.spring.injection.annot.SpringBean;

import web.PawShopSession;
import web.pages.article.EditArticlePage;
import web.pages.base.ArticleLinkPanel;
import web.pages.secured.SecuredPage;

@SuppressWarnings("serial")
public class ProfileArticlePage extends SecuredPage {

	@SpringBean
	private ArticleRepo articleRepoInstance;
	
	@SpringBean
	private UserRepo userRepoInstance;

	@SpringBean
	private EntityResolver entityResolver;

	public ProfileArticlePage() {
		PawShopSession s = PawShopSession.get();
		UserClient user = userRepoInstance.getByUsername(s.getUsername());
		add(new Label("usernameArticles", new StringResourceModel("usernameArticles", this, new EntityModel<UserClient>(UserClient.class, user))));
		add(new RefreshingView<Article>("articles", new EntityModel<UserClient>(UserClient.class, user)) {
			private static final long serialVersionUID = 1L;

			@Override
			protected Iterator<IModel<Article>> getItemModels() {
				
				List<IModel<Article>> result = new ArrayList<IModel<Article>>();
				for (Article a : ((UserClient) getDefaultModelObject()).getOwnedArticles()) {
					final int id = a.getId();
					result.add(new LoadableDetachableModel<Article>() {
						private static final long serialVersionUID = 1L;

						@Override
						protected Article load() {
							return entityResolver.fetch(Article.class, id);
						}
					});
				}
				return result.iterator();
			}

			@Override
			protected void populateItem(Item<Article> item) {
				item.add(new ArticleLinkPanel("articleLink", item.getModel()));
				Link<EditArticlePage> editLink = new BookmarkablePageLink<EditArticlePage>("edit", EditArticlePage.class, 
						new PageParameters().add("articleId", item.getModel().getObject().getId()));
				item.add(editLink);
				Link<Article> suspendLink = new Link<Article>("suspend", item.getModel()){
					@Override
					public void onClick() {
						PawShopSession s = PawShopSession.get();
						UserClient user = userRepoInstance.getByUsername(s.getUsername());
						user.suspend(getModelObject());
					}
					
					@Override
					protected void onBeforeRender() {
						setVisible(!getModelObject().getSuspended());
						super.onBeforeRender();
					}
				};
				item.add(suspendLink);
				Link<Article> unsuspendLink = new Link<Article>("unsuspend", item.getModel()){
					@Override
					public void onClick() {
						PawShopSession s = PawShopSession.get();
						UserClient user = userRepoInstance.getByUsername(s.getUsername());
						user.unsuspend(getModelObject());
					}
					
					@Override
					protected void onBeforeRender() {
						setVisible(getModelObject().getSuspended());
						super.onBeforeRender();
					}
				};
				item.add(unsuspendLink);
			}
		});
	}
}
