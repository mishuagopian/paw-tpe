package web.pages.userPrivateProfile;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import models.EntityModel;
import models.EntityResolver;
import models.Query;
import models.QueryRepo;
import models.UserClient;
import models.UserRepo;

import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.markup.repeater.RefreshingView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import web.PawShopSession;
import web.pages.article.QueryPanel;
import web.pages.secured.SecuredPage;

@SuppressWarnings("serial")
public class PendingQueriesPage extends SecuredPage {

	@SpringBean
	private EntityResolver entityResolver;
	
	@SpringBean
	private UserRepo userRepoInstance;
	
	@SpringBean
	private QueryRepo queryRepoInstance;
	
	public PendingQueriesPage() {
		PawShopSession session = PawShopSession.get();
		String username = session.getUsername();
		UserClient user = userRepoInstance.getByUsername(username);
		
		add(new RefreshingView<Query>("queries", new EntityModel<UserClient>(
				UserClient.class, user)) {

			private static final long serialVersionUID = 1L;

			@Override
			protected Iterator<IModel<Query>> getItemModels() {
				List<IModel<Query>> result = new ArrayList<IModel<Query>>();
				for (Query q : queryRepoInstance.getPendingQueriesFrom((UserClient)getDefaultModel().getObject())) {
					final int id = q.getId();
					result.add(new LoadableDetachableModel<Query>() {
						private static final long serialVersionUID = 1L;

						@Override
						protected Query load() {
							return entityResolver.fetch(Query.class, id);
						}
					});
				}
				return result.iterator();
			}

			@Override
			protected void populateItem(Item<Query> item) {
				item.add(new QueryPanel("query", item.getModel()));
			}
		});
	}
}
