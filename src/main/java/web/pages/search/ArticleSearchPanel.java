package web.pages.search;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import models.Article;
import models.ArticleRepo;
import models.EntityResolver;
import models.QueryObject;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.markup.repeater.RefreshingView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import web.pages.article.ArticleCategoriesPanel;
import web.pages.base.ArticleLinkPanel;
import web.pages.base.DatePanel;

@SuppressWarnings("serial")
public class ArticleSearchPanel extends Panel {

	@SpringBean
	private ArticleRepo articleRepoInstance;
	
	@SpringBean
	private EntityResolver entityResolver;
	
	private transient QueryObject queryObject;
	
	public ArticleSearchPanel(String id, QueryObject queryObject) {
		super(id);
		this.queryObject = queryObject;
		if (this.queryObject != null) {
			add(new RefreshingView<Article>("articles") {
				@Override
				protected Iterator<IModel<Article>> getItemModels() {
					List<IModel<Article>> result = new ArrayList<IModel<Article>>();
					for (Article a : articleRepoInstance.search(getQueryObject())) {
						final int id = a.getId(); 
						result.add(new LoadableDetachableModel<Article>() {

							@Override
							protected Article load() {
								return entityResolver.fetch(Article.class, id);
							}
						});
					}
					return result.iterator();
				}
				@Override
				protected void populateItem(Item<Article> item) {
					item.add(new ArticleLinkPanel("articleLink", item.getModel()));
					item.add(new ArticleCategoriesPanel("articleCategories", item.getModel()));
					item.add(new Label("articlePrice", new PropertyModel<Article>(item.getModel(), "price")));
					item.add(new DatePanel("articlePublicationDate", new PropertyModel<Date>(item.getModel(), "publicationDate")));
				}
			});
		}
	}
	
	@Override
	protected void onBeforeRender() {
		setVisible(queryObject != null);
		super.onBeforeRender();
	}
	
	private QueryObject getQueryObject() {
		return queryObject;
	}
	
}
