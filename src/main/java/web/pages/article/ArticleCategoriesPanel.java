package web.pages.article;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import models.Article;
import models.Category;
import models.EntityResolver;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.markup.repeater.RefreshingView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

@SuppressWarnings("serial")
public class ArticleCategoriesPanel extends Panel {

	@SpringBean
	private EntityResolver entityResolver;
	
	public ArticleCategoriesPanel(String id, IModel<Article> model) {
		super(id);
		initialize(model);
	}

	private void initialize(final IModel<Article> model) {
		add(new RefreshingView<Category>("category") {
			private static final long serialVersionUID = 1L;

			@Override
			protected Iterator<IModel<Category>> getItemModels() {
				List<IModel<Category>> result = new ArrayList<IModel<Category>>();
				for (Category c : model.getObject().getCategories()) {
					final int id = c.getId(); 
					result.add(new LoadableDetachableModel<Category>() {
						private static final long serialVersionUID = 1L;

						@Override
						protected Category load() {
							return entityResolver.fetch(Category.class, id);
						}
					});
				}
				return result.iterator();
			}

			@Override
			protected void populateItem(Item<Category> item) {
				item.add(new Label("categoryName", new PropertyModel<String>(item.getModel(), "name")));
			}
		});
	}
	
}
