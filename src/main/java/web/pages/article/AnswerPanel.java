package web.pages.article;

import models.EntityResolver;
import models.Query;
import models.UserRepo;

import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.ResourceModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.apache.wicket.validation.validator.StringValidator.MaximumLengthValidator;

import web.PawShopSession;

@SuppressWarnings("serial")
public class AnswerPanel extends Panel {
	
	@SpringBean
	private EntityResolver entityResolver;

	@SpringBean
	private UserRepo userRepoInstance;
	
	private transient String answer;
	
	public AnswerPanel(String id, IModel<Query> model) {
		super(id);
		setDefaultModel(new CompoundPropertyModel<Query>(model));
		initialize(model);
	}

	private void initialize(IModel<Query> model) {
		Form<AnswerPanel> form = new Form<AnswerPanel>("answerForm", new CompoundPropertyModel<AnswerPanel>(this)) {
			@Override
			protected void onSubmit() {
				PawShopSession s = PawShopSession.get();
				userRepoInstance.getByUsername(s.getUsername()).answer(query(), answer);
			}
		};
		form.add(new TextField<String>("answer").add(new MaximumLengthValidator(Query.ANSWER_MAX_SIZE)).setRequired(true));
		form.add(new Button("submit", new ResourceModel("submit")));
		add(form);
	}
	
	@Override
	protected void onBeforeRender() {
		PawShopSession s = PawShopSession.get();
		setVisible(query().getAnswer()==null && s.isSignedIn() && query().getArticle().getOwner().getUsername().equals(s.getUsername()));
		super.onBeforeRender();
	}
	
	private Query query(){
		return (Query)getDefaultModelObject();
	}
	
}
