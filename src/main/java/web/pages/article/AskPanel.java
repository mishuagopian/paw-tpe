package web.pages.article;



import models.Article;
import models.ArticleRepo;
import models.EntityResolver;
import models.Query;
import models.UserClient;
import models.UserRepo;

import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.ResourceModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.apache.wicket.validation.validator.StringValidator.MaximumLengthValidator;

import web.PawShopSession;

@SuppressWarnings("serial")
public class AskPanel extends Panel {
	
	@SpringBean
	private EntityResolver entityResolver;

	@SpringBean
	private UserRepo userRepoInstance;
	
	@SpringBean
	private ArticleRepo articleRepoInstance;
	
	private transient Article article;
	private transient String query;
	
	public AskPanel(String id, IModel<Article> model) {
		super(id);
		initialize(model);
		article = model.getObject();
	}

	private void initialize(IModel<Article> model) {
		Form<AskPanel> form = new Form<AskPanel>("askForm", new CompoundPropertyModel<AskPanel>(this)) {
			@Override
			protected void onSubmit() {
				PawShopSession s = PawShopSession.get();
				UserClient user = userRepoInstance.getByUsername(s.getUsername());
				Article art = articleRepoInstance.getById(article.getId());
				user.ask(art, query);
			}
		};
		form.add(new TextField<String>("query").add(new MaximumLengthValidator(Query.QUERY_MAX_SIZE)).setRequired(true));
		form.add(new Button("ask", new ResourceModel("ask")));
		add(form);
	}
	
	@Override
	protected void onBeforeRender() {
		PawShopSession s = PawShopSession.get();
		setVisible(s.isSignedIn() && !article.getOwner().getUsername().equals(s.getUsername()));
		super.onBeforeRender();
	}
}
