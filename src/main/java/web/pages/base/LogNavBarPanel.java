package web.pages.base;

import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.Panel;

import web.PawShopSession;
import web.pages.article.PublishArticlePage;
import web.pages.home.HomePage;
import web.pages.userPrivateProfile.UserPrivateProfilePage;

@SuppressWarnings("serial")
public class LogNavBarPanel extends Panel {
	
	public LogNavBarPanel(String id) {
		super(id);
		add(new Link<Void>("signOut") {
			@Override
			public void onClick() {
				PawShopSession s = PawShopSession.get();
				s.signOut();
				setResponsePage(HomePage.class);
			}
		});
		
		add(new Link<Void>("profile") {
			@Override
			public void onClick() {
				setResponsePage(UserPrivateProfilePage.class);
			}
		});
		
		add(new Link<Void>("publish") {
			@Override
			public void onClick() {
				setResponsePage(PublishArticlePage.class);
			}
		});
	}
	
	@Override
	protected void onBeforeRender() {
		PawShopSession s = PawShopSession.get();
		this.setVisible(s.isSignedIn());
		super.onBeforeRender();
	}
}
