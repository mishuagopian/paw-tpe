package web.pages.base;

import models.UserRepo;

import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import web.PawShopSession;
import web.pages.admin.AdminProfilePage;

@SuppressWarnings("serial")
public class AdminNavBarPanel extends Panel {

	@SpringBean
	private UserRepo userRepoInstance;
	
	public AdminNavBarPanel(String id) {
		super(id);
		add(new Link<Void>("admin") {
			@Override
			public void onClick() {
				setResponsePage(AdminProfilePage.class);
			}
		});
	}
	
	@Override
	protected void onBeforeRender() {
		PawShopSession s = PawShopSession.get();
		this.setVisible(s.isAdmin(userRepoInstance));
		super.onBeforeRender();
	}
}
