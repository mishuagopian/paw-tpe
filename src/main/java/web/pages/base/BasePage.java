package web.pages.base;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;

import web.pages.home.HomePage;
import web.pages.search.SearchPage;

@SuppressWarnings("serial")
public class BasePage extends WebPage {

	public BasePage() {		
		LogNavBarPanel lp = new LogNavBarPanel("login");
		NotLogNavBarPanel nlp = new NotLogNavBarPanel("logout");
		AdminNavBarPanel ap = new AdminNavBarPanel("admin");
		add(lp);
		add(nlp);
		add(ap);
		add(new BookmarkablePageLink<HomePage>("home", HomePage.class));
		add(new Link<Void>("search") {
			@Override
			public void onClick() {
				setResponsePage(new SearchPage(null));
			}
		});
	}
	
}
