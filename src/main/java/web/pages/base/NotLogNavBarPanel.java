package web.pages.base;

import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.panel.Panel;

import web.PawShopSession;
import web.pages.recoverPassword.RecoverPasswordPage;
import web.pages.signIn.SignInPage;
import web.pages.signUp.SignUpPage;

@SuppressWarnings("serial")
public class NotLogNavBarPanel extends Panel {

	public NotLogNavBarPanel(String id) {
		super(id);
		add(new BookmarkablePageLink<SignInPage>("signIn", SignInPage.class));
		add(new BookmarkablePageLink<SignUpPage>("signUp", SignUpPage.class));
		add(new BookmarkablePageLink<RecoverPasswordPage>("recoverPassword", RecoverPasswordPage.class));
	}

	@Override
	protected void onBeforeRender() {
		PawShopSession s = PawShopSession.get();
		this.setVisible(!s.isSignedIn());
		super.onBeforeRender();
	}
	
}
