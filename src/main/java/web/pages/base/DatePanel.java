package web.pages.base;

import java.util.Date;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.ocpsoft.pretty.time.PrettyTime;

@SuppressWarnings("serial")
public class DatePanel extends Panel {

	public DatePanel(String id, IModel<Date> model) {
		super(id);
		PrettyTime p = new PrettyTime();
		String enhancedDate = p.format(model.getObject());
		add(new Label("date", enhancedDate));
	}
	
}
