package web.pages.home;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import models.ArticleRepo;
import models.Category;
import models.CategoryRepo;
import models.EntityResolver;
import models.QueryObject;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.markup.repeater.RefreshingView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import web.pages.base.BasePage;
import web.pages.search.SearchPage;

@SuppressWarnings("serial")
public class HomePage extends BasePage {
	
	@SpringBean
	private CategoryRepo categoryRepoInstance;
	
	@SpringBean
	private ArticleRepo articleRepoInstance;
	
	@SpringBean
	private EntityResolver entityResolver;
	
	public HomePage(){
		add(new RefreshingView<Category>("category") {
			private static final long serialVersionUID = 1L;

			@Override
			protected Iterator<IModel<Category>> getItemModels() {
				List<IModel<Category>> result = new ArrayList<IModel<Category>>();
				for (Category c : categoryRepoInstance.getAll()) {
					final int id = c.getId(); 
					result.add(new LoadableDetachableModel<Category>() {
						private static final long serialVersionUID = 1L;

						@Override
						protected Category load() {
							return entityResolver.fetch(Category.class, id);
						}
					});
				}
				return result.iterator();
			}

			@Override
			protected void populateItem(Item<Category> item) {
				Link<Category> link =new Link<Category>("categoryLink", item.getModel()) {
					@Override
					public void onClick() {
						QueryObject qo = new QueryObject();
						qo.setCategory((Category)getDefaultModelObject());
						setResponsePage(new SearchPage(qo));
					}
				};
				link.add(new Label("name", new PropertyModel<String>(item.getModel(), "name")));
				item.add(link);
			}
		});
		add(new Link<Void>("allCategories") {
			@Override
			public void onClick() {
				setResponsePage(new SearchPage(new QueryObject()));
			}
		});
		
		add(new RecentlyPublishedPanel("recentlyPublishedPanel"));
		add(new MostVisitedPanel("mostVisitedPanel"));
		add(new MostPurchasedPanel("mostPurchasedPanel"));
		add(new SellersPanel("sellersPanel"));
		
	}

}
