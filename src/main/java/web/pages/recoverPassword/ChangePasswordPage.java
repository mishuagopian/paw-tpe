package web.pages.recoverPassword;

import models.EntityModel;
import models.UserClient;
import models.UserRepo;

import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.PasswordTextField;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.ResourceModel;
import org.apache.wicket.model.StringResourceModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.apache.wicket.validation.validator.StringValidator.MaximumLengthValidator;

import web.pages.base.BasePage;
import web.pages.signIn.SignInPage;

@SuppressWarnings("serial")
public class ChangePasswordPage extends BasePage {

	@SpringBean
	private UserRepo userRepoInstance;
	
	private transient String newPassword;
	private transient String newPasswordRepeat;
	
	public ChangePasswordPage(PageParameters params) {
		UserClient user = userRepoInstance.getByUsername(params.get("username").toString());
		if (user == null) {
			setResponsePage(RecoverPasswordPage.class);
		}
		setDefaultModel(new CompoundPropertyModel<UserClient>(new EntityModel<UserClient>(UserClient.class, user)));
		
		add(new FeedbackPanel("feedback"));
		Form<ChangePasswordPage> form = new Form<ChangePasswordPage>("newPasswordForm", 
				new CompoundPropertyModel<ChangePasswordPage>(this)) {
			@Override
			protected void onSubmit() {
				if (newPassword.equals(newPasswordRepeat)) {
					userClient().setPassword(newPassword);
					setResponsePage(SignInPage.class);
				} else {
					error(new StringResourceModel("passwordsMustCoincide", this, null).getString());
				}
			}
		};
		form.add(new PasswordTextField("newPassword").add(new MaximumLengthValidator(UserClient.PASSWORD_MAX_SIZE)).setRequired(true));
		form.add(new PasswordTextField("newPasswordRepeat").add(new MaximumLengthValidator(UserClient.PASSWORD_MAX_SIZE)).setRequired(true));
		form.add(new Button("submit", new ResourceModel("submit")));
		add(form);
	}

	private UserClient userClient(){
		return (UserClient)getDefaultModelObject();
	}
	
}