package web.pages.recoverPassword;

import models.EntityModel;
import models.EntityResolver;
import models.UserClient;
import models.UserRepo;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.model.ResourceModel;
import org.apache.wicket.model.StringResourceModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.apache.wicket.validation.validator.StringValidator.MaximumLengthValidator;

import web.pages.base.BasePage;

@SuppressWarnings("serial")
public class SecretAnswerPage extends BasePage {
	
	@SpringBean
	private UserRepo userRepoInstance;
	
	@SpringBean
	private EntityResolver entityResolver;
	
	private transient String secretAnswer;
	
	public SecretAnswerPage(PageParameters params) {
		UserClient user = userRepoInstance.getByUsername(params.get("username").toString());
		if (user == null) {
			setResponsePage(RecoverPasswordPage.class);
		}
		setDefaultModel(new CompoundPropertyModel<UserClient>(new EntityModel<UserClient>(UserClient.class, user)));
		
		add(new Label("secretQuestion", new PropertyModel<String>(new EntityModel<UserClient>(UserClient.class, user), "secretQuestion")));
		
		add(new FeedbackPanel("feedback"));
		Form<SecretAnswerPage> form = new Form<SecretAnswerPage>("secretAnswerForm", 
				new CompoundPropertyModel<SecretAnswerPage>(this)) {
			@Override
			protected void onSubmit() {
				if (userClient().getSecretQuestionAnswer().equals(secretAnswer)) {
					setResponsePage(ChangePasswordPage.class, new PageParameters().add("username", userClient().getUsername()));
				} else {
					error(new StringResourceModel("invalidSecretAnswer", this, null).getString());
				}
			}
		};
		form.add(new TextField<String>("secretAnswer").add(new MaximumLengthValidator(UserClient.SECRET_ANSWER_MAX_SIZE)).setRequired(true));
		form.add(new Button("submit", new ResourceModel("submit")));
		add(form);
	}
	
	private UserClient userClient(){
		return (UserClient)getDefaultModelObject();
	}
	
}
