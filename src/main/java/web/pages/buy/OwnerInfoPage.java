package web.pages.buy;



import models.Article;
import models.EntityModel;
import models.UserClient;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.PropertyModel;

import web.pages.secured.SecuredPage;

@SuppressWarnings("serial")
public class OwnerInfoPage extends SecuredPage {

	public OwnerInfoPage(IModel<Article> model) {
		add(new Label("name1",new PropertyModel<String>(new EntityModel<UserClient>(UserClient.class, model.getObject().getOwner()), "firstName")));
		add(new Label("name2",new PropertyModel<String>(new EntityModel<UserClient>(UserClient.class, model.getObject().getOwner()), "lastName")));
		add(new Label("email",new PropertyModel<String>(new EntityModel<UserClient>(UserClient.class, model.getObject().getOwner()), "email")));
	}
	
}
