package web;

import models.UserClient;
import models.UserRepo;

import org.apache.wicket.Session;
import org.apache.wicket.protocol.http.WebSession;
import org.apache.wicket.request.Request;

@SuppressWarnings("serial")
public class PawShopSession extends WebSession {
	
	private String username;

	public static PawShopSession get() {
		return (PawShopSession) Session.get();
	}

	public PawShopSession(Request request) {
		super(request);
	}

	public String getUsername() {
		return username;
	}

	public boolean signIn(String username, String password, UserRepo users) {
		UserClient user = users.getByUsername(username);
		if (user != null && user.checkPassword(password)) {
			this.username = username;
			user.access();
			return true;
		}
		return false;
	}

	public boolean isSignedIn() {
		return username != null;
	}
	
	public boolean isAdmin(UserRepo users) {
		if(username == null){
			return false;
		}
		UserClient user = users.getByUsername(username);
		if (user != null && user.getAdmin()) {
			return true;
		}
		return false;
	}

	public void signOut() {
		username = null;
        invalidate();
        clear();
	}
}
